<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="./resources/logo.png" alt="Project logo"></a>
</p>

<h3 align="center">Wireguard Attack</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![Gitlab Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://gitlab.limos.fr/borlonjo/wireguard-attack/-/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://gitlab.limos.fr/borlonjo/wireguard-attack/-/merge_requests)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> This repository is a POC demonstrating the compromission of a Wireguard connection allowing to discover identity of a user.
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

This repo contain a python script to conduct an attack on a Wireguard connection. The server is running inside a docker on a Debian Bookworm amd64 virtual machine. All other VM are running on the same image.

We are using Vagrant along with Virtualbox to setup the experiment. We will create 4 VM (server, client1, client 2, MitM attacker).

`wireguard-{server,client1,client2,attacker}` directories will be *"mounted"* respectively on each VM so content of repository is available inside VM.

## 🏁 Getting Started <a name = "getting_started"></a>

Here is what is needed to operate the attack.

### Prerequisites

You need to have VirtualBox installed on your machine along with Hashicorp Vagrant.

```bash
sudo apt install virtualbox vagrant
```

### Installing

A step by step series of examples that tell you how to get a development env running.

Say what the step will be

```bash
git clone https://gitlab.limos.fr/borlonjo/wireguard-attack.git
cd wireguard-attack
```

Now you can start VM using vagrant

```bash
vagrant up
```

End with an example of getting some data out of the system or using it for a little demo.

<!-- ## 🔧 Running the tests <a name = "tests"></a>

Explain how to run the automated tests for this system.

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
``` -->

## 🎈 Usage <a name="usage"></a>

To connect to VM using ssh, you can issue:

```bash
vagrant ssh <vm_name>
```

To execute the script you need to connect to wireguard-attacker VM:

```bash
vagrant ssh wireguard-attacker
cd /srv/wireguard-attacker
source .venv/bin/activate
./attack
```

To initiate Wireguard connection, you need to connect to wireguard-clientN VM:

```bash
vagrant ssh wireguard-client1
```

To start Wireguard connection (as root):

```bash
wg-quick up wg0
```

To stop Wireguard connection (as root):

```bash
wg-quick down wg0
```

### Sniff using Wireshark (optional)

To start sniffing using Wireshark, you need to start dumpcap on attacker VM and pipe data to your wireshark installation. In order to do so you need to retrieve vagrant ssh configuration :

```bash
vagrant ssh-config

[...]

Host wireguard-attacker
  HostName 127.0.0.1
  User vagrant
  Port 2202
  UserKnownHostsFile /dev/null
  StrictHostKeyChecking no
  PasswordAuthentication no
  IdentityFile /home/user/wireguard-attack/.vagrant/machines/wireguard-attacker/virtualbox/private_key
  IdentitiesOnly yes
  LogLevel FATAL
  PubkeyAcceptedKeyTypes +ssh-rsa
  HostKeyAlgorithms +ssh-rsa
```

You need IdentityFile value and Port value of **wireguard-attacker** host to use these in this command :

```bash
ssh -o StrictHostKeyChecking=no -i <IdentityFile>  vagrant@localhost -p <Port> sudo dumpcap -i eth1 -w - | wireshark -k -i -
```

For MacOS users, you may need to replace `wireshark` by the full path of your installed binary.

<!-- ## 🚀 Deployment <a name = "deployment"></a>

Add additional notes about how to deploy this on a live system.

## ⛏️ Built Using <a name = "built_using"></a>

- [MongoDB](https://www.mongodb.com/) - Database
- [Express](https://expressjs.com/) - Server Framework
- [VueJs](https://vuejs.org/) - Web Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- [@kylelobo](https://github.com/kylelobo) - Idea & Initial work

See also the list of [contributors](https://github.com/kylelobo/The-Documentation-Compendium/contributors) who participated in this project.

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- Hat tip to anyone whose code was used
- Inspiration
- References -->
