Vagrant.configure("2") do |config|
  # Using latest Debian amd64 image
  config.vm.box = "generic/alpine318"

  # Define three virtual machines with specific names and IP addresses
  vm_configurations = [
    { name: "wireguard-server", ip: "10.13.37.10" },
    { name: "wireguard-client1", ip: "10.13.37.11" },
    { name: "wireguard-client2", ip: "10.13.37.12" },
    { name: "wireguard-attacker", ip: "10.13.37.13" }
  ]

  vm_configurations.each do |vm_config|
    config.vm.define vm_config[:name] do |node|

      # Naming VMs
      node.vm.hostname = vm_config[:name]

      # Configure a private network with a specific IP address
      node.vm.network "private_network", ip: vm_config[:ip], virtualbox__intnet: true

      # Customize VM system settings
      node.vm.provider "virtualbox" do |vb|
        vb.name = vm_config[:name]
        vb.memory = "1024"
        vb.cpus = 1
      end

      # Mount local directories on VMs
      if vm_config[:name] == "wireguard-client1"
        node.vm.synced_folder "./wireguard-client1", "/etc/wireguard"
        # node.vm.synced_folder "./linux", "/linux"

        node.vm.provider "virtualbox" do |vb|
          vb.memory = "4096"
          vb.cpus = 8
        end
      end
      if vm_config[:name] == "wireguard-client2"
        node.vm.synced_folder "./wireguard-client2", "/etc/wireguard"
      end
      if vm_config[:name] == "wireguard-server"
        node.vm.synced_folder "./wireguard-server", "/etc/wireguard"
      end
      if vm_config[:name] == "wireguard-attacker"
        node.vm.synced_folder "./wireguard-attacker", "/srv/wireguard-attacker"
      end

      # Allow Promiscuous Mode on attacker machine
      if vm_config[:name] == "wireguard-attacker"
        node.vm.box = "debian/bookworm64"
        node.vm.provider "virtualbox" do |vb|
          vb.customize ['modifyvm', :id, '--nicpromisc2', 'allow-all']
        end
      end

      # Shell provisioning
      if vm_config[:name] == "wireguard-server"
        node.vm.provision "shell", inline: <<-SHELL
          #!/bin/bash
          echo "Hello from #{vm_config[:name]} with IP: #{vm_config[:ip]}!"

          # Update repository
          sudo apk update

          # Install wireguard along with
          sudo apk add wireguard-tools-wg-quick iptables

          # Some customizations
          sudo apk add vim

          # Boot server
          sudo wg-quick up wg0
        SHELL
      end

      if vm_config[:name] == "wireguard-client1"
        node.vm.provision "shell", inline: <<-SHELL
          #!/bin/bash
          echo "Hello from #{vm_config[:name]} with IP: #{vm_config[:ip]}!"

          # Update repository
          sudo apk update

          # Install wireguard along with
          sudo apk add wireguard-tools-wg-quick iptables

          # Some customizations
          sudo apk add vim
          sudo apk add git alpine-sdk
          sudo addgroup vagrant abuild
        SHELL
        node.vm.provision :reload
        node.vm.provision "shell", privileged: false, inline: <<-SHELL
          #!/bin/bash

          # Clone kernel sources
          test -d ~/aports || git clone https://github.com/alpinelinux/aports --branch 3.18-stable
        SHELL
        node.vm.provision "file", source: "./linux/wireguard-leak.patch", destination: "~/aports/main/linux-lts/wireguard-leak.patch"
        node.vm.provision "file", source: "./linux/APKBUILD", destination: "~/aports/main/linux-lts/APKBUILD"
        node.vm.provision "shell", privileged: false, inline: <<-SHELL
          #!/bin/bash
          # Reinstall kernel with customized wireguard module
          abuild-keygen -a -i -n
          cd aports/main/linux-lts && abuild checksum && abuild -r -k && sudo apk add ~/packages/main/x86_64/linux-virt-6.1.77-r0.apk && sudo reboot
        SHELL
      end

      if vm_config[:name] == "wireguard-client2"
        node.vm.provision "shell", inline: <<-SHELL
          #!/bin/bash
          echo "Hello from #{vm_config[:name]} with IP: #{vm_config[:ip]}!"

          # Update repository
          sudo apk update

          # Install wireguard along with
          sudo apk add wireguard-tools-wg-quick iptables

          # Some customizations
          sudo apk add vim
        SHELL
      end

      if vm_config[:name] == "wireguard-attacker"
        node.vm.provision "shell", inline: <<-SHELL
          #!/bin/bash
          echo "Hello from #{vm_config[:name]} with IP: #{vm_config[:ip]}!"

          # Set interface on promiscuous mode at system level
          sudo ip link set eth1 promisc on

          # Update repository
          sudo apt-get -y update

          # Install tshark (dumpcap)
          echo "wireshark-common wireshark-common/install-setuid boolean true" | sudo debconf-set-selections
          sudo apt-get -y install tshark python3-pip virtualenv libpcap-dev

          # Add vagrant user to wireshark group
          sudo adduser vagrant wireshark

          # Some customizations
          sudo apt-get -y install vim
          sed -ri 's/^( *)#alias (.*)/\\1alias \\2/' /home/vagrant/.bashrc

          # Set virtualenv and install attack dependencies
          if [[ ! -d /srv/wireguard-attacker/.venv ]]; then
            cd /srv/wireguard-attacker
            virtualenv .venv
            pip install -r requirements.txt
          fi
        SHELL
      end
    end
  end
end
