--- a/drivers/net/wireguard/noise.c	2022-12-11 22:15:18.000000000 +0000
+++ b/drivers/net/wireguard/noise.c	2024-02-12 13:01:27.834662422 +0000
@@ -25,6 +25,14 @@
  * <- e, ee, se, psk, {}
  */
 
+void printHex(const char *varName, const u8 *ptr, size_t size) {
+    printk("%s = \'", varName);
+    for (size_t i = 0; i < size; i++) {
+        printk(KERN_CONT "%.2x", ptr[i]);
+    }
+    printk(KERN_CONT "\'");
+}
+
 static const u8 handshake_name[37] = "Noise_IKpsk2_25519_ChaChaPoly_BLAKE2s";
 static const u8 identifier_name[34] = "WireGuard v1 zx2c4 Jason@zx2c4.com";
 static u8 handshake_init_hash[NOISE_HASH_LEN] __ro_after_init;
@@ -309,6 +317,10 @@ static void hmac(u8 *out, const u8 *in,
 	u8 i_hash[BLAKE2S_HASH_SIZE] __aligned(__alignof__(u32));
 	int i;
 
+	// printHex("[HMAC] out", out, BLAKE2S_HASH_SIZE);
+	// printHex("[HMAC] in", in, inlen);
+	// printHex("[HMAC] key", key, keylen);
+
 	if (keylen > BLAKE2S_BLOCK_SIZE) {
 		blake2s_init(&state, BLAKE2S_HASH_SIZE);
 		blake2s_update(&state, key, keylen);
@@ -358,6 +370,7 @@ static void kdf(u8 *first_dst, u8 *secon
 
 	/* Extract entropy from data into secret */
 	hmac(secret, data, chaining_key, data_len, NOISE_HASH_LEN);
+        //printHex("[KDF] secret", secret, BLAKE2S_HASH_SIZE);
 
 	if (!first_dst || !first_len)
 		goto out;
@@ -365,6 +378,7 @@ static void kdf(u8 *first_dst, u8 *secon
 	/* Expand first key: key = secret, data = 0x1 */
 	output[0] = 1;
 	hmac(output, output, secret, 1, BLAKE2S_HASH_SIZE);
+        //printHex("[KDF] output", output, BLAKE2S_HASH_SIZE+1);
 	memcpy(first_dst, output, first_len);
 
 	if (!second_dst || !second_len)
@@ -373,6 +387,7 @@ static void kdf(u8 *first_dst, u8 *secon
 	/* Expand second key: key = secret, data = first-key || 0x2 */
 	output[BLAKE2S_HASH_SIZE] = 2;
 	hmac(output, output, secret, BLAKE2S_HASH_SIZE + 1, BLAKE2S_HASH_SIZE);
+        //printHex("[KDF] output2", output, BLAKE2S_HASH_SIZE+1);
 	memcpy(second_dst, output, second_len);
 
 	if (!third_dst || !third_len)
@@ -394,8 +409,9 @@ static void derive_keys(struct noise_sym
 			const u8 chaining_key[NOISE_HASH_LEN])
 {
 	u64 birthdate = ktime_get_coarse_boottime_ns();
-	kdf(first_dst->key, second_dst->key, NULL, NULL,
-	    NOISE_SYMMETRIC_KEY_LEN, NOISE_SYMMETRIC_KEY_LEN, 0, 0,
+	const u8 absurd_value = 0x55;
+	kdf(first_dst->key, second_dst->key, NULL, &absurd_value,
+	    NOISE_SYMMETRIC_KEY_LEN, NOISE_SYMMETRIC_KEY_LEN, 0, 1,
 	    chaining_key);
 	first_dst->birthdate = second_dst->birthdate = birthdate;
 	first_dst->is_valid = second_dst->is_valid = true;
@@ -410,6 +426,9 @@ static bool __must_check mix_dh(u8 chain
 
 	if (unlikely(!curve25519(dh_calculation, private, public)))
 		return false;
+    // printHex("dh_calculation", dh_calculation, NOISE_PUBLIC_KEY_LEN);
+    // printHex("private", private, NOISE_PUBLIC_KEY_LEN);
+    // printHex("public", public, NOISE_PUBLIC_KEY_LEN);
 	kdf(chaining_key, key, NULL, dh_calculation, NOISE_HASH_LEN,
 	    NOISE_SYMMETRIC_KEY_LEN, 0, NOISE_PUBLIC_KEY_LEN, chaining_key);
 	memzero_explicit(dh_calculation, NOISE_PUBLIC_KEY_LEN);
@@ -423,6 +442,9 @@ static bool __must_check mix_precomputed
 	static u8 zero_point[NOISE_PUBLIC_KEY_LEN];
 	if (unlikely(!crypto_memneq(precomputed, zero_point, NOISE_PUBLIC_KEY_LEN)))
 		return false;
+    printHex("precomputed", precomputed, NOISE_PUBLIC_KEY_LEN);
+    // printHex("private", private, NOISE_PUBLIC_KEY_LEN);
+    // printHex("public", public, NOISE_PUBLIC_KEY_LEN);
 	kdf(chaining_key, key, NULL, precomputed, NOISE_HASH_LEN,
 	    NOISE_SYMMETRIC_KEY_LEN, 0, NOISE_PUBLIC_KEY_LEN,
 	    chaining_key);
@@ -456,6 +478,7 @@ static void handshake_init(u8 chaining_k
 			   const u8 remote_static[NOISE_PUBLIC_KEY_LEN])
 {
 	memcpy(hash, handshake_init_hash, NOISE_HASH_LEN);
+    // printHex("Hi", handshake_init_hash, NOISE_HASH_LEN);
 	memcpy(chaining_key, handshake_init_chaining_key, NOISE_HASH_LEN);
 	mix_hash(hash, remote_static, NOISE_PUBLIC_KEY_LEN);
 }
@@ -467,6 +490,7 @@ static void message_encrypt(u8 *dst_ciph
 	chacha20poly1305_encrypt(dst_ciphertext, src_plaintext, src_len, hash,
 				 NOISE_HASH_LEN,
 				 0 /* Always zero for Noise_IK */, key);
+    // printHex("dst_ciphertext", dst_ciphertext, noise_encrypted_len(src_len));
 	mix_hash(hash, dst_ciphertext, noise_encrypted_len(src_len));
 }
 
@@ -536,35 +560,52 @@ wg_noise_handshake_create_initiation(str
 
 	handshake_init(handshake->chaining_key, handshake->hash,
 		       handshake->remote_static);
+    // printHex("C_i", handshake->chaining_key, NOISE_HASH_LEN);
+    // printHex("H_i", handshake->hash, NOISE_HASH_LEN);
 
 	/* e */
 	curve25519_generate_secret(handshake->ephemeral_private);
 	if (!curve25519_generate_public(dst->unencrypted_ephemeral,
 					handshake->ephemeral_private))
 		goto out;
+    printHex("E^priv_i", handshake->ephemeral_private, NOISE_PUBLIC_KEY_LEN);
+    // printHex("E^pub_i", dst->unencrypted_ephemeral, NOISE_PUBLIC_KEY_LEN);
+
 	message_ephemeral(dst->unencrypted_ephemeral,
 			  dst->unencrypted_ephemeral, handshake->chaining_key,
 			  handshake->hash);
+    // printHex("dst->unencrypted_ephemeral", dst->unencrypted_ephemeral, NOISE_PUBLIC_KEY_LEN);
+    // printHex("handshake->chaining_key", handshake->chaining_key, NOISE_HASH_LEN);
+    // printHex("handshake->hash", handshake->hash, NOISE_HASH_LEN);
 
 	/* es */
 	if (!mix_dh(handshake->chaining_key, key, handshake->ephemeral_private,
 		    handshake->remote_static))
 		goto out;
+    // printHex("key", key, NOISE_SYMMETRIC_KEY_LEN);
+    // printHex("handshake->chaining_key", handshake->chaining_key, NOISE_HASH_LEN);
 
 	/* s */
 	message_encrypt(dst->encrypted_static,
 			handshake->static_identity->static_public,
 			NOISE_PUBLIC_KEY_LEN, key, handshake->hash);
+    // printHex("dst->encrypted_static", dst->encrypted_static, (size_t)(noise_encrypted_len(dst->encrypted_static)));
+    // printHex("handshake->hash", handshake->hash, NOISE_HASH_LEN);
 
 	/* ss */
 	if (!mix_precomputed_dh(handshake->chaining_key, key,
 				handshake->precomputed_static_static))
 		goto out;
+    // printHex("key", key, NOISE_SYMMETRIC_KEY_LEN);
+    // printHex("handshake->chaining_key", handshake->chaining_key, NOISE_HASH_LEN);
 
 	/* {t} */
 	tai64n_now(timestamp);
 	message_encrypt(dst->encrypted_timestamp, timestamp,
 			NOISE_TIMESTAMP_LEN, key, handshake->hash);
+    // printHex("timestamp", timestamp, NOISE_TIMESTAMP_LEN);
+    // printHex("dst->encrypted_timestamp", dst->encrypted_timestamp, NOISE_TIMESTAMP_LEN);
+    // printHex("handshake->hash", handshake->hash, NOISE_HASH_LEN);
 
 	dst->sender_index = wg_index_hashtable_insert(
 		handshake->entry.peer->device->index_hashtable,
@@ -600,18 +641,28 @@ wg_noise_handshake_consume_initiation(st
 		goto out;
 
 	handshake_init(chaining_key, hash, wg->static_identity.static_public);
+    // printHex("C_i", handshake->chaining_key, NOISE_HASH_LEN);
+    // printHex("H_i", handshake->hash, NOISE_HASH_LEN);
 
 	/* e */
 	message_ephemeral(e, src->unencrypted_ephemeral, chaining_key, hash);
+    // printHex("dst->unencrypted_ephemeral", src->unencrypted_ephemeral, NOISE_PUBLIC_KEY_LEN);
+    // printHex("chaining_key", chaining_key, NOISE_HASH_LEN);
+    // printHex("hash", hash, NOISE_HASH_LEN);
 
 	/* es */
 	if (!mix_dh(chaining_key, key, wg->static_identity.static_private, e))
 		goto out;
+    // printHex("key", key, NOISE_SYMMETRIC_KEY_LEN);
+    // printHex("chaining_key", chaining_key, NOISE_HASH_LEN);
+    // printHex("E", e, NOISE_PUBLIC_KEY_LEN);
+    // printHex("wg->static_identity.static_private", wg->static_identity.static_private, NOISE_PUBLIC_KEY_LEN);
 
 	/* s */
 	if (!message_decrypt(s, src->encrypted_static,
 			     sizeof(src->encrypted_static), key, hash))
 		goto out;
+    // printHex("hash", hash, NOISE_HASH_LEN);
 
 	/* Lookup which peer we're actually talking to */
 	peer = wg_pubkey_hashtable_lookup(wg->peer_hashtable, s);
@@ -689,6 +740,7 @@ bool wg_noise_handshake_create_response(
 	if (!curve25519_generate_public(dst->unencrypted_ephemeral,
 					handshake->ephemeral_private))
 		goto out;
+
 	message_ephemeral(dst->unencrypted_ephemeral,
 			  dst->unencrypted_ephemeral, handshake->chaining_key,
 			  handshake->hash);
@@ -765,17 +817,29 @@ wg_noise_handshake_consume_response(stru
 
 	/* e */
 	message_ephemeral(e, src->unencrypted_ephemeral, chaining_key, hash);
+    // printHex("src->unencrypted_ephemeral", src->unencrypted_ephemeral, NOISE_PUBLIC_KEY_LEN);
+    // printHex("chaining_key", chaining_key, NOISE_HASH_LEN);
+    // printHex("hash", hash, NOISE_HASH_LEN);
 
 	/* ee */
 	if (!mix_dh(chaining_key, NULL, ephemeral_private, e))
 		goto fail;
+    // printHex("ephemeral_private", ephemeral_private, NOISE_PUBLIC_KEY_LEN);
+    // printHex("e", e, NOISE_PUBLIC_KEY_LEN);
+    // printHex("chaining_key", chaining_key, NOISE_HASH_LEN);
 
 	/* se */
 	if (!mix_dh(chaining_key, NULL, wg->static_identity.static_private, e))
 		goto fail;
+    // printHex("wg->static_identity.static_private", wg->static_identity.static_private, NOISE_PUBLIC_KEY_LEN);
+    // printHex("e", e, NOISE_PUBLIC_KEY_LEN);
+    // printHex("chaining_key", chaining_key, NOISE_HASH_LEN);
 
 	/* psk */
 	mix_psk(chaining_key, hash, key, preshared_key);
+    // printHex("chaining_key", chaining_key, NOISE_HASH_LEN);
+    // printHex("key", key, NOISE_SYMMETRIC_KEY_LEN);
+    // printHex("hash", hash, NOISE_HASH_LEN);
 
 	/* {} */
 	if (!message_decrypt(NULL, src->encrypted_nothing,
@@ -831,6 +895,7 @@ bool wg_noise_handshake_begin_session(st
 					  HANDSHAKE_CONSUMED_RESPONSE;
 	new_keypair->remote_index = handshake->remote_index;
 
+	printk("Before derive");
 	if (new_keypair->i_am_the_initiator)
 		derive_keys(&new_keypair->sending, &new_keypair->receiving,
 			    handshake->chaining_key);
@@ -838,6 +903,9 @@ bool wg_noise_handshake_begin_session(st
 		derive_keys(&new_keypair->receiving, &new_keypair->sending,
 			    handshake->chaining_key);
 
+     // printHex("new_keypair->sending", new_keypair->sending.key, NOISE_SYMMETRIC_KEY_LEN);
+     // printHex("new_keypair->receiving", new_keypair->receiving.key, NOISE_SYMMETRIC_KEY_LEN);
+
 	handshake_zero(handshake);
 	rcu_read_lock_bh();
 	if (likely(!READ_ONCE(container_of(handshake, struct wg_peer,
